# Utility functions
import os
import subprocess
import shutil

def create_project(levels, directory='project', style="simple", force=False):
    if os.path.exists(directory) and force:
        shutil.rmtree(directory)

    if style == "fan":
        level_str = "--fan-levels"
    elif style == "simple":
        level_str = "--project-levels"
    else:
        raise ValueError("style: {} ain't legit".format(style))

    # Create the project
    subprocess.run(
        [
            "bstgen",
            style,
            level_str,
            str(levels),
            directory
        ]
    )


def time_buildstream(command, workdir):
    proc = subprocess.run(
        [
            "/usr/bin/time",
            "--format",
            "*** %e %M",
            "bash",
            "-c",
            "{}".format(" ".join(command)),
        ],
        cwd=workdir,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )

    result = {}

    decoded = proc.stdout.decode("utf-8")
    for line in decoded.split("\n"):
        if line.startswith("*** "):
            seconds, max_memory = line.strip("*").strip().split(" ")
            return {
                "max_memory": int(max_memory),
                "clock_time": float(seconds),
            }

    raise Exception("Result unable to be parsed successfully", result)


def clear_cache():
    cache = os.path.join(os.environ["XDG_CACHE_HOME"], "buildstream")
    if not os.path.exists(cache):
        return
    shutil.rmtree(cache)
    assert not os.path.exists(cache)
