#!/usr/bin/env python3
import json
import os

import click
import numpy as np

import utils

os.environ["XDG_CACHE_HOME"] = os.path.join(os.getcwd(), "cache")


@click.command()
@click.option("--builders", type=str, default='4',
              help='number of builders')
@click.option("--repeats", type=int, default=3,
              help='How many times to repeat each test')
@click.option("--levels", type=int, default=10,
              help='number of project levels to generate')
@click.option("--step-size", type=int, default=1,
              help='Step size between the levels')
@click.option("--results-file",
              type=click.Path(file_okay=True, writable=True),
              default=os.path.join(os.getcwd(), "results.json"),
              help='The file to write the results to')
@click.argument("command")
def main(
        builders,
        repeats,
        levels,
        step_size,
        results_file,
        command
):
    command_list = ["bst", "--builders", builders, command]

    # Generate the results
    generate_results(command_list,
                     levels=levels,
                     step_size=step_size,
                     style="fan",
                     repeats=repeats,
                     results_file=results_file)


def generate_results(command,
                     style="simple",
                     levels=10,
                     step_size=1,
                     workspace="project",
                     repeats=3,
                     results_file="results.json"):

    # Create a list of levels separated by their step size
    sizes = list(range(1, int(levels) + 1, int(step_size)))

    to_write = []
    for i, size in enumerate(sizes):
        data = {"measurements": []}
        print("*** {}/{} ***".format(i+1, len(sizes)))

        # Create the project
        utils.create_project(size, directory=workspace, style=style, force=True)

        elements = len(os.listdir(os.path.join(workspace, 'elements'))) - 1  # -1 because of the stack
        data["elements"] = elements

        # Repeat the test n times and take measurements
        print("Benchmarking {} elements".format(elements))
        print("Measurements:")
        for iteration in range(0, repeats):
            utils.clear_cache()
            result = utils.time_buildstream(command, workspace)
            data["measurements"].append(result)
            print("\t{}/{}: {}".format(iteration + 1, repeats, result))

        times = []
        mems = []
        for measurement in data["measurements"]:
            times.append(measurement["clock_time"])
            mems.append(measurement["max_memory"])

        data["mean_clock_time"] = round(sum(times)/len(times), 2)
        data["mean_max_memory"] = round(sum(mems)/len(mems), 0)

        data["median_clock_time"] = np.median(times)
        data["median_max_memory"] = np.median(mems)

        print("*************\n")

        to_write.append(data)

    # Dump the data to a file
    with open(results_file, 'w') as outfile:
        json.dump(to_write, outfile, indent=4)

    # Dump the results to a file for plotting
    to_plot = {"elements": [], "mean_clock_times": [], "mean_max_memories": [], "median_clock_times": [], "median_max_memories": [] }
    for results in to_write:
        to_plot["elements"].append(results["elements"])

        to_plot["mean_clock_times"].append(results["mean_clock_time"])
        to_plot["mean_max_memories"].append(results["mean_max_memory"])

        to_plot["median_clock_times"].append(results["median_clock_time"])
        to_plot["median_max_memories"].append(results["median_max_memory"])

    with open("plot.json", 'w') as outfile:
        json.dump(to_plot, outfile, indent=4)


if __name__ == "__main__":
    main()
