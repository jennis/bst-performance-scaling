# Testing BuildStream's performance as a project scales

This simple test script tests how BuildStream performs as the number of elements
in a project increases.

There are two scripts you need to know about:
1. performance_vs_growth.py - generates .json files of the data

2. plot_results.py - Takes a json file and plots a graph


## To generate the results:

1. Ensure that you have bstgen and BuildStream installed.

2. `./performance_vs_growth.py show` or `./performance_vs_growth.py build` to generate results for `bst show` and `bst build`, respectively.

3. Look at the options ("./performance_vs_growth --help") to play around with "levels", "step_size" and "builders".

Note that `--repeats` defaults to 3. This is how many times we should repeat measurements for each test.

## To plot:

1. Ensure that you have matplotlib installed

2. ./plot_results.py results.json
