#!/usr/bin/env python3

#  Copyright Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Antoine Wacheux <awacheux@bloomberg.net>
#        William Salmon <will.salmon@codethink.co.uk>

import os
from os import path
import copy

class Element():
    ElementCount = 0
    def __init__(self, project, name=None, dependents=[]):
        self.Index = copy.copy(Element.ElementCount)
        Element.ElementCount += 1

        self.project = project

        if name:
            self.name = name
        else:
            self.name = self.Index

        self.dependents = dependents

    def create(self):
        project_dir = path.join(self.project.output_directory, "files", "project_{}".format(self.name))
        os.makedirs(project_dir)

        for file in range(0, self.project.num_file):
            file_dir = path.join(project_dir, "file_{}".format(file))

            with open(file_dir, mode='wb') as f:
                f.write(bytes(self.project.file_size))

        bst_file = (
            "kind: import\n"
            "sources:\n"
            "  - kind: local\n"
            "    path: files/project_{project}\n"
            "config:\n"
            "  target: /project_{project}\n".format(project=self.name))
        if self.dependents: bst_file += "depends:\n"
        for dependent in self.dependents:
            bst_file += "  - filename: project_{number}.bst\n    type: build\n".format(number=dependent.name)

        with open(path.join(self.project.output_directory, self.project.element_dir, "project_{}.bst".format(self.name)), mode='w', encoding='utf-8') as bst:
            bst.write(bst_file)

class Project():
    def __init__(self, output_directory, element_dir, *, projectname='bstgen_project', file_size=1024, num_file=1): 
        self.file_size = file_size
        self.num_file = num_file

        self.output_directory = output_directory
        os.makedirs(output_directory)
        self.element_dir = element_dir
        os.makedirs(path.join(output_directory, element_dir))

        project_conf = "name: {}\nelement-path: {}/".format(projectname, element_dir)

        with open(path.join(output_directory, "project.conf"), mode='w', encoding='utf-8') as project_file:
            project_file.write(project_conf)



def bstgen_simple(output_directory, num_file, file_size, project_levels, project_widths, shape):
    """
    This creates a set of elements that are arranged in to `project_levels` of elements
    were every level has `project_widths` number of elements in it and every element
    depends on every element in the level beneath.

    This creates projects that can easily contain elements with large numbers of 
    dependencies this are very hard for bst to process and are unrepresentative of real
    projects so use this with caution.
    """
    element_dir = "elements"
    project_object = Project(output_directory, element_dir, file_size=file_size, num_file=num_file)


    lastelements = []
    for level in range(project_levels):
        levelelements = []
        if shape == 'grow':
            thiswidth = int(1 + (project_widths - 1)/(project_levels - 1) * level)
        else:
            thiswidth = project_widths
        for width in range(thiswidth):
            ele = Element(project_object, dependents=lastelements)
            ele.create()
            levelelements.append(ele)
        lastelements = copy.copy(levelelements)
    

    bst_stack = ("kind: stack\n"
                   "depends:\n")
    for project in lastelements:
        bst_stack += "  - filename: project_{}.bst\n    type: build\n\n".format(project.name)

    with open(path.join(output_directory, element_dir, "build_all.bst"), mode="w", encoding="utf-8") as build_all:
        build_all.write(bst_stack)


def single_fan(origin, levels, project_object):
    lastelements = [origin]
    for level in range(2, levels + 1):
        levelelements = []
        for index in range(level):
            pIIl = max(index - 1, 0)
            pIIu = max(1, min(index + 1, level - 1))
            ele = Element(project_object, dependents = lastelements[pIIl:pIIu])
            ele.create()
            levelelements.append(ele)
        lastelements = copy.copy(levelelements)
    return lastelements


def stacked_fans(pre_level, levels, fan_levels, project_object):
    next_level = []
    for element in pre_level:
        next_level += single_fan(element, fan_levels, project_object)
    levels_left = levels - 1
    if levels_left:
        return stacked_fans(next_level, levels_left, fan_levels, project_object)
    return next_level


def bstgen_fan(output_directory, num_file, file_size, fan_levels, fan_numbers):
    """
    This creates a project which starts with one element and then creates `fan_levels`
    of levels of elements with the number of elements in each level one more than the
    previous level. Each element depends on 2 elements from the level below unless it
    is the first or last element in the level in which case it depends on the first or
    element from the level below respectively.
    """
    element_dir = "elements"
    project_object = Project(output_directory, element_dir, file_size=file_size, num_file=num_file)

    root_element = Element(project=project_object)
    root_element.create()

    lastelements = stacked_fans([root_element], fan_numbers, fan_levels, project_object)

    bst_stack = ("kind: stack\n"
                   "depends:\n")
    for project in lastelements:
        bst_stack += "  - filename: project_{}.bst\n    type: build\n\n".format(project.name)

    with open(path.join(output_directory, element_dir, "build_all.bst"), mode="w", encoding="utf-8") as build_all:
        build_all.write(bst_stack)
