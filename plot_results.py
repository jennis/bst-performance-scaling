#!/usr/bin/env python3

import json
import os

import click
import matplotlib.pyplot as plt

@click.command()
@click.option("--title",
              type=str,
              default="bst performance scaling",
              help="Title for the graphs")
@click.argument("results",
              type=click.Path(file_okay=True, writable=True),
              default=os.path.join(os.getcwd(), "results.json"))
def plot_results(title, results):

    # Load the data
    assert os.path.exists(results)
    with open(results) as json_data:
        results = json.load(json_data)

    # Plot the results
    fig = plt.figure(1)
    ax = fig.add_subplot(121)
    ax.set_title(title)
    ax.plot(results["no_of_elements"], results["clock_time"], "ro-")
    ax.set_xlabel("Number of elements")
    ax.set_ylabel("Clock time (s)")
    ax.set_ylim(bottom=0, top=max(results["clock_time"])*1.1)

    ax = fig.add_subplot(122)
    ax.set_title(title)
    ax.plot(results["no_of_elements"], results["max_memory"], "bo-")
    ax.set_xlabel("Number of elements")
    ax.set_ylabel("Max memory (K)")
    minimum = min(results["max_memory"])
    ax.set_ylim(bottom=0, top=max(results["max_memory"])*1.1)
    plt.show()

if __name__ == "__main__":
    plot_results()
